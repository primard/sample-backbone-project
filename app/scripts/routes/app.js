/*global define*/

define([
    'jquery',
    'backbone',
    'views/client',
    'views/clients',
    'views/home',
    'views/stats',
], function ($, Backbone, ClientView, ClientsView, HomeView, StatsView) {
    'use strict';

    var AppRouter = Backbone.Router.extend({
        routes: {
        	'home': 'home',
            'clients/new' : 'new_client',
        	'clients': 'clients',
        	'birthstats': 'stats'
        },
        
        'initialize': function() {
	        this.home();
        },
        
		'home': function() {
	        this.loadView(new HomeView());
        },
        
        'stats': function() {
	        this.loadView(new StatsView());
        },

        'new_client': function() {
            this.loadView(new ClientView({mode: 'create'}));
        },

        'clients': function() {
        	this.loadView(new ClientsView());
        },

        loadView : function(view) {
            if(this.view) {
                // rather than calling view.remove() that removes the container from the DOM
                this.view.undelegateEvents();
                this.view.$el.empty();
                this.view.unbind();
            }

            this.view = view;
            this.view.render();
        }
    });

    return AppRouter;
});
