/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'collections/stats'
], function ($, _, Backbone, JST, StatsCollection) {
    'use strict';
    
    var mois_francais = new Array('Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre');
    
    var timenow = new Date().getTime();
    var d = new Date(timenow);

    var StatsView = Backbone.View.extend({
        template: JST['app/scripts/templates/stats.ejs'],
        el: '#main_content',
		current_month: mois_francais[d.getMonth()],
        events: {
        	'click #submit': 'submitForm'
        },

        initialize: function() {
			_.bindAll(this, "render");
            _.bindAll(this, "submitForm");

			this.stats = new StatsCollection();
            this.stats.bind('reset', this.render);

            this.stats.fetch({
                success: function (collection, response) {
                    console.log('fetch success');
                    console.log(response);
                },
                reset: true
            });
        },

        render: function() {
            this.$el.html(this.template({stats: this.stats}));
		    return this;
        },

        submitForm: function(e) {
        	e.preventDefault();

        	var form = $(this.el).find('#StatMater');
        	
        	
        	this.stats.forEach(function(stat){
	        	stat.set({
					maternity_code: form.find("#maternity_code_" + stat.get('id')).val(),
					real_number: form.find("#real_number_" + stat.get('id')).val(),
	        	});       
	        });

            Backbone.sync('create', this.stats);
        }
    });

    return StatsView;
});