/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'models/client'
], function ($, _, Backbone, JST, ClientModel) {
    'use strict';

    var ClientView = Backbone.View.extend({
        template: JST['app/scripts/templates/client.ejs'],
        edit_template: JST['app/scripts/templates/client_edit.ejs'],
        el: '#main_content',

        events: {
        	'click #submit': 'submitForm'
        },

        initialize: function() {
            _.bindAll(this, "render");
            _.bindAll(this, "renderEdit");
            _.bindAll(this, "renderShow");

            if(this.options.mode == 'create') {
            	this.model = new ClientModel();
            } else {
            	this.model = this.options.model;
            }
        },

        render: function() {
            if(this.options.mode == 'create' || this.options.mode == 'edit') {
                this.renderEdit();
            } else {
                this.renderShow();
            }
        },

        renderEdit: function() {
            this.$el.html(this.edit_template({mode: this.options.mode, model: this.model.toJSON()}));
		    return this;
        },

        renderShow: function() {
            this.$el.html(this.template({model: this.model.toJSON()}));
		    return this;
        },

        submitForm: function(e) {
        	e.preventDefault();

        	console.log('save');

        	var form = $(this.el).find('#client_form');
        	console.log(form.find("#firstname").val());

        	this.model.set({
        		firstname: form.find("#prenom").val(),
				lastname: form.find("#nom").val(),
				address1: form.find("#adresse").val(),
	        	address2: form.find("#complement").val(),
	        	postal_code: form.find("#codeP").val(),
	        	city: form.find("#ville").val(),
	        	country: form.find("#pays").val(),
	        	home_phone_number: form.find("#telfixe").val(),
	        	mobile_phone_number: form.find("#telmob2").val(),
	        	profesionnal_phone_number: form.find("#telpro").val(),
	        	email: form.find("#email").val(),
	        	gender: form.find("#civilite").val(),
	        	optin_company: form.find("#optin_company").is(":checked"),
	        	optin_partners: form.find("#optin_partners").is(":checked"),
	        	optout_company: form.find("#optout_company").is(":checked"),
	        	optout_partners: form.find("#optout_partners").is(":checked"),
        	});

			this.model.get('baby').set({
        		firstname: form.find("#baby_firstname").val(),
				lastname: form.find("#baby_lastname").val(),
				birth_date: form.find("#birth_date").val(),
        	});

        	this.model.save(null, {
        		success: function() {
        			console.log('save success');
        			Backbone.history.navigate("/clients", { trigger: true });
        		},
        		error: function() {
        			console.log('save error');
        		}
        	});
        }
    });

    return ClientView;
});