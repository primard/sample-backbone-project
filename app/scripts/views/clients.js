/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates',
    'collections/client'
], function ($, _, Backbone, JST, ClientCollection) {
    'use strict';

    var ClientsView = Backbone.View.extend({
        template: JST['app/scripts/templates/clients.ejs'],
        el: '#main_content',

        initialize: function() {
            _.bindAll(this, "render");
            _.bindAll(this, "markLoadingFinished");

            var self = this;

            this.isLoading = true;

            this.clients = new ClientCollection();
            this.clients.bind('reset', this.markLoadingFinished);
            this.clients.bind('reset', this.render);

            this.clients.fetch({
                success: function (collection, response) {
                    console.log('fetch success');
                    console.log(response);
                },
                reset: true
            });
        },

        markLoadingFinished : function() {
            console.log('mark loading finished');
            this.isLoading = false;
        },

        render : function () {
            console.log('render clients view');
            this.$el.html(this.template({clients: this.clients, isLoading: this.isLoading}));
		    return this;
        }
    });

    return ClientsView;
});
