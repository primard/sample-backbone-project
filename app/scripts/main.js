/*global require*/
'use strict';

require.config({
    shim: {
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: [
                'underscore',
                'jquery'
            ],
            exports: 'Backbone'
        }
    },
    paths: {
        jquery: '../bower_components/jquery/jquery',
        backbone: '../bower_components/backbone/backbone',
        underscore: '../bower_components/underscore/underscore'
    }
});

require([
    'backbone',
    'routes/app'
], function (Backbone, AppRouter) {

    window['APP'] = {
        BASE_URL: 'http://172.24.1.17:8080/api-baby/'
        // BASE_URL: 'http://localhost:8080/'
    };

    var client_router = new AppRouter();

    Backbone.history.start();
});
