/*global define*/

define([
    'underscore',
    'backbone',
    'models/stats'
], function (_, Backbone, StatsModel) {
    'use strict';

    var StatsModel = Backbone.Model.extend({
        defaults: {
            id: null,
            maternity_code: null,
            real_number: null,
        },
        
        validation: {
        },

        url: function() { 
            return APP.BASE_URL + 'multiples/birthstats'; 
        },
    });

    return StatsModel;
});
