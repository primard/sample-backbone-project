/*global define*/

define([
    'underscore',
    'backbone',
    'models/baby'
], function (_, Backbone, BabyModel) {
    'use strict';

    var ClientModel = Backbone.Model.extend({
        defaults: {
            id : null,
        	firstname: null,
        	lastname: null,
        	address1: null,
        	address2: null,
        	postal_code: null,
        	city: null,
        	country: null,
        	home_phone_number: null,
        	mobile_phone_number: null,
        	profesionnal_phone_number: null,
        	email: null,
        	gender: null,
        	optin_company: null,
        	optin_partners: null,
        	optout_company: null,
        	optout_partners: null,
        	
            baby: new BabyModel(),
        },
        
        validation: {
	        email: {
		        required: true,
		        pattern: 'email'
	        }
        },

        url: function() { 
            return APP.BASE_URL + 'prospect/client'; 
        },
    });

    return ClientModel;
});