/*global define*/

define([
    'underscore',
    'backbone'
], function (_, Backbone) {
    'use strict';

    var BabyModel = Backbone.Model.extend({
        defaults: {
        	id: null,
        	firstname: null,
        	lastname: null,
        }
    });

    return BabyModel;
});
