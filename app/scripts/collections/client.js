/*global define*/

define([
    'underscore',
    'backbone',
    'models/client'
], function (_, Backbone, ClientModel) {
    'use strict';

    var ClientCollection = Backbone.Collection.extend({
        model: ClientModel,
        url : function() { 
        	return APP.BASE_URL + 'prospect/clients'; 
        },
        // parse: function(json) {
        // 	console.log('JSON');
        //     console.log(json);
        // }
    });

    return ClientCollection;
});
