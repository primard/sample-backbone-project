/*global define*/

define([
    'underscore',
    'backbone',
    'models/stats'
], function (_, Backbone, StatsModel) {
    'use strict';

    var StatsCollection = Backbone.Collection.extend({
        model: StatsModel,
        url : function() { 
        	return APP.BASE_URL + 'birthstats'; 
        },

        // parse: function(json) {
        // 	console.log('JSON');
        //     console.log(json);
        // }
    });

    return StatsCollection;
});